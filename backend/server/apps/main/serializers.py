from rest_framework import serializers
from .models import Team


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=100)


class SignupSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=100)
    rule = serializers.IntegerField()


class TeamSerializer(serializers.ModelSerializer):
    candidates = serializers.StringRelatedField(many=True)

    class Meta:
        model = Team
        fields = "__all__"