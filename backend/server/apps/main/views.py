from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import LoginSerializer,SignupSerializer
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import Profile
from packs.constants import http,rules
from packs.auth import pjwt


class Signup(APIView):

    def post(self,request):
        serializer = SignupSerializer(data=request.data)
        if not serializer.is_valid():
            return Response("Bad data format", http.HTTP_BAD_REQ)
        if User.objects.filter(username=serializer.validated_data["username"]).count() != 0:
            return Response("Username already exists", http.HTTP_BAD_REQ)
        data = serializer.validated_data
        user = User()
        user.username = data["username"]
        user.set_password(data["password"])
        user.save()
        profile = Profile()
        profile.user = user
        profile.rule = data["rule"]
        profile.save()
        return Response("Registration was successful", http.HTTP_OK)


class Login(APIView):

    def post(self,request):
        serializer = Login(data=request.data)
        if not serializer.is_valid():
            return Response("Bad data format", http.HTTP_BAD_REQ)
        data = serializer.validated_data
        user = authenticate(username=data["username"],password=data["password"])
        if user == None:
            return Response("Username or password is wrong", http.HTTP_UNAUTHORIZED)
        token = pjwt.generate(user.username)
        return Response(token, http.HTTP_OK)

    
class CreateTeam(APIView):
    
    @pjwt.authenticate
    def post(self,request):
        if request.user.profile.rule != rules.MENTOR
            return Response("Only mentors can create team", http.HTTP_UNAUTHORIZED)
        serializer = Login(data=request.data)
        if not serializer.is_valid():
            return Response("Bad data format", http.HTTP_BAD_REQ)
        #TODO CreateTeam