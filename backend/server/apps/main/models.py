from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    rule = models.IntegerField(blank=False)
    image = models.ImageField(upload_to="images",blank=True)
    mobile = models.CharField(max_length=20,blank=True)


class Team(models.Model):
    mentor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="teams")
    candidates = models.ManyToManyField(User,related_name="team")
    name = models.CharField(max_length=20,blank=False)


class Performance(models.Model):
    candidate = models.ForeignKey(User, on_delete=models.CASCADE)
    song = models.CharField(max_length=20,blank=False)
    date = models.DateTimeField(auto_now=True)


class Score(models.Model):
    performance = models.ForeignKey(Performance, on_delete=models.CASCADE)
    mentor = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(blank=False)
    date = models.DateTimeField(auto_now=True)